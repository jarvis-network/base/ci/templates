variable "CI_REGISTRY_IMAGE" {}
variable "CI_COMMIT_SHA" {}

target "test" {
  dockerfile = "./test/Dockerfile"

  output     = ["type=registry"]

  tags       = ["${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"]

  platforms  = ["linux/amd64"]

  target     = "test"

  cache-from = [ "type=registry,ref=${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}" ]
  cache-to   = [ "type=registry,ref=${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}" ]
}
